pub static AUTO_PUB_LIB_TAG:&str = "// auto-publib";
pub static STRUCTURE_OPENING_TAGS:[&str; 2] = ["impl", "trait"];
pub static MAX_FUNCTIONS_PER_CATEGORY:f32 = 6.0;
pub static TEST_CONFIG_START_TAG:&str = "#[cfg(";
pub static TEST_CONFIG_END_TAG:&str = ")";