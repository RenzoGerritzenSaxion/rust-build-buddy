static mut LOGS:Vec<LogType> = Vec::new();

enum LogType {
	Error(String),
	Warning(String),
	Log(String)
}
impl std::fmt::Display for LogType {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		f.write_str(match self {
			LogType::Error(message) => message,
			LogType::Warning(message) => message,
			LogType::Log(message) => message
		})
	}
}

/// Create a new message from warning data.
fn create_warning_data(message:&str, source_file:&str, source_line:&str, source_line_index:usize, col_start:usize, col_length:usize) -> String {

	// Prepare required data.
	let source_line_padding_len:usize = source_line.len() - source_line.trim_start().len();
	let padding:String = " ".repeat(source_line_padding_len + 1);
	let trimmed_source_line:&str = source_line.trim();
	let trimmed_col_start:usize = col_start - source_line_padding_len;
	let source_underline_arrows:String = " ".repeat(trimmed_col_start) + &"^".repeat(col_length);
	let source_line_index:usize = source_line_index + 1;

	// Create message string.
	[
		message.to_owned(),
		format!("{padding}--> {source_file}:{source_line_index}:{trimmed_col_start}"),
		format!("{padding} |"),
		format!("{source_line_index} | {trimmed_source_line}"),
		format!("{padding} | {source_underline_arrows}"),
		format!("{padding} |")
	].join("\n")
}

/// Add a log to the static LOGS stack.
#[allow(dead_code)]
pub fn log(message:&str) {
	unsafe { LOGS.push(LogType::Log(message.to_owned())); }
}

/// Add a warning to the static LOGS stack.
#[allow(dead_code)]
pub fn warn(message:&str, source_file:&str, source_line:&str, source_line_index:usize, col_start:usize, col_length:usize) {
	let message_data:String = create_warning_data(message, source_file, source_line, source_line_index, col_start, col_length);
	unsafe { LOGS.push(LogType::Warning(message_data)); }
}

/// Add an error to the static LOGS stack.
#[allow(dead_code)]
pub fn error(message:&str, source_file:&str, source_line:&str, source_line_index:usize, col_start:usize, col_length:usize) {
	let message_data:String = create_warning_data(message, source_file, source_line, source_line_index, col_start, col_length);
	unsafe { LOGS.push(LogType::Error(message_data)); }
}

/// Print and clear the stack of logs.
pub fn print() {
	use std::process::exit;

	unsafe {

		// If there are any errors, print them and exit with status code 1.
		let errors:Vec<String> = LOGS.iter().filter(|log| matches!(log, LogType::Error(_))).map(|log| log.to_string()).collect::<Vec<String>>();
		if !errors.is_empty() {
			for error in &errors {
				println!("{error}");
			}
			exit(1);
		}

		// If there are any warnings, print them and exit with status code 1.
		let warnings:Vec<String> = LOGS.iter().filter(|log| matches!(log, LogType::Warning(_))).map(|log| log.to_string()).collect::<Vec<String>>();
		if !warnings.is_empty() {
			for warning in &warnings {
				println!("{warning}");
			}
			exit(1);
		}

		// Print logs.
		let logs:Vec<String> = LOGS.iter().filter(|log| matches!(log, LogType::Log(_))).map(|log| log.to_string()).collect::<Vec<String>>();
		for log in &logs {
			println!("{}", log);
		}
	}
}