use crate::{ statics::AUTO_PUB_LIB_TAG, file_operations::{ read_file, write_file, filename_of, filename_no_ext_of, enumerate_dir }, code_parsing::find_between };

pub fn auto_publib(mod_files:&Vec<String>) {

	// Loop through 'lib.rs' and 'mod.rs' files.
	for file in mod_files {

		// Check if file contains autoinclude tag.
		let file_contents:String = read_file(file);
		if file_contents.contains(AUTO_PUB_LIB_TAG) {

			// Get the string before the auto-include tag.
			let pre_tag:&str = file_contents.split(AUTO_PUB_LIB_TAG).collect::<Vec<&str>>()[0].trim();

			// Read settings from auto-include tag.
			let settings_string:String = find_between(&format!("{file_contents}\n"), AUTO_PUB_LIB_TAG, "\n").unwrap_or_default().trim().to_string();
			let setting_sub_import:bool = settings_string.contains("subimport");
			let setting_no_use:bool = settings_string.contains("no-use");
			let setting_pub_mod:bool = settings_string.contains("pub-mod");
			let setting_super_only:bool = settings_string.contains("super-only");
			let setting_crate_only:bool = settings_string.contains("crate-only");
			let mut setting_exclusions:Vec<String> = vec![];
			if settings_string.contains("exclude=[") {
				setting_exclusions = find_between(&settings_string, "exclude=[", "]").unwrap_or_default().trim().split(',').map(|s| s.to_string()).collect::<Vec<String>>();
			}

			// Automatically collect imports.
			let file_dir:String = file.split('/').collect::<Vec<&str>>()[..file.split('/').count()-1].join("/");
			let mut include_libs:Vec<String> = enumerate_dir(&file_dir, true, false, false, &|_:&str| true);
			let mut include_files:Vec<String> = enumerate_dir(&file_dir, false, true, false, &|file_path:&str| {
				let name:String = filename_of(file_path);
				name.ends_with(".rs") && &name != "lib.rs" && &name != "mod.rs" && &name != "main.rs"
			});

			// Filter out exclusions.
			for exclusion in &setting_exclusions {
				include_libs = include_libs.iter().filter(|dir| !dir.contains(exclusion)).map(|dir| dir.to_string()).collect::<Vec<String>>();
				include_files = include_files.iter().filter(|file| !file.contains(exclusion)).map(|file| file.to_string()).collect::<Vec<String>>();
			}

			// Adapt imports to settings.
			let mut pub_mod_list:String = include_libs.iter().map(|lib_name| format!("pub mod {};", filename_no_ext_of(lib_name))).collect::<Vec<String>>().join("\n");
			let mut sub_import_list:String = String::new();
			let mut mod_list:String = include_files.iter().map(|file_name| format!("mod {};", filename_no_ext_of(file_name))).collect::<Vec<String>>().join("\n");
			let mut use_list:String = include_files.iter().map(|file_name| format!("pub use {}::*;", filename_no_ext_of(file_name))).collect::<Vec<String>>().join("\n");
			if setting_no_use {
				use_list = String::new();
			}
			if setting_pub_mod {
				mod_list = mod_list.split('\n').map(|mod_usage| format!("pub {mod_usage}")).collect::<Vec<String>>().join("\n");
			}
			if setting_sub_import {
				sub_import_list = include_libs.iter().map(|lib_name| format!("pub use {}::*;", filename_no_ext_of(lib_name))).collect::<Vec<String>>().join("\n");
			}
			if setting_super_only {
				pub_mod_list = pub_mod_list.replace("pub ", "pub(super) ");
				use_list = use_list.replace("pub ", "pub(super) ");
			}
			if setting_crate_only {
				pub_mod_list = pub_mod_list.replace("pub ", "pub(crate) ");
				use_list = use_list.replace("pub ", "pub(crate) ");
			}

			// Create the new contents of the file.
			let mut new_contents:String = format!("{}\n\n{} {}\n{}\n\n{}\n\n{}\n\n{}",
				pre_tag,
				AUTO_PUB_LIB_TAG,
				settings_string,
				pub_mod_list,
				sub_import_list,
				mod_list,
				use_list
			).trim().to_owned();
			while new_contents.contains("\n\n\n") {
				new_contents = new_contents.replace("\n\n\n", "\n\n");
			}

			// Write new imports to file.
			if new_contents != file_contents {
				write_file(file, &new_contents);
			}
		}
	}
}