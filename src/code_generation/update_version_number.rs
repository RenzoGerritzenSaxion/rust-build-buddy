use crate::file_operations::{ read_file, write_file };
use regex::Regex;

/// Update the main crates version number by 0.0.1.
pub fn update_version_number() {

	// Files.
	let toml_file:&str = "Cargo.toml";
	let lock_file:&str = "Cargo.lock";
	let toml_contents:String = read_file(toml_file);
	let lock_contents:String = read_file(lock_file);

	// Build regexes.
	let regex_toml_project_name:Regex = Regex::new(r#"\[package\]([^\[]+)name(\s+)?=(\s+)?\"(?<Name>\S+)\""#).expect("Could not parse regex expression");
	let project_name:&str = regex_toml_project_name.captures(&toml_contents).expect("Could not get project name from toml file").name("Name").unwrap().as_str();
	let regex_toml_version:Regex = Regex::new(r#"\[package\]([^\[]+)version(\s+)?=(\s+)?\"(?<Version>\d+\.\d+\.\d+)\""#).expect("Could not parse regex expression");
	let regex_lock_version:Regex = Regex::new(&format!("{}{}{}", r#"\[package\]([^\[]+)name(\s+)?=(\s+)?\""#, project_name, r#"\"[^\[]+(\s+)?=(\s+)?\"(?<Version>\d+\.\d+\.\d+)\""#)).expect("Could not parse regex expression");

	// Match the toml and lock versions.
	let toml_capture:regex::Match<'_> = regex_toml_version.captures(&toml_contents).expect("Could not get version number from toml file").name("Version").unwrap();
	let lock_capture:regex::Match<'_> = regex_lock_version.captures(&lock_contents).expect("Could not get version number from lock file").name("Version").unwrap();

	// Create new version number.
	let mut new_version_number:Vec<u32> = toml_capture.as_str().split('.').map(|number| number.parse::<u32>().unwrap()).collect::<Vec<u32>>();
	*new_version_number.last_mut().unwrap() += 1;
	let new_version:String = new_version_number.iter().map(|number| number.to_string()).collect::<Vec<String>>().join(".");

	// Write new version number.
	let new_toml_contents:String = format!("{}{}{}", &toml_contents[..toml_capture.start()], new_version, &toml_contents[toml_capture.end()..]);
	let new_lock_contents:String = format!("{}{}{}", &lock_contents[..lock_capture.start()], new_version, &lock_contents[lock_capture.end()..]);
	write_file(toml_file, &new_toml_contents);
	write_file(lock_file, &new_lock_contents);

	// Validate successful write. If not, panic, do not wait for anything else.
	let toml_contents:String = read_file(toml_file);
	let lock_contents:String = read_file(lock_file);
	if regex_toml_version.captures(&toml_contents).expect("Could not get version number from toml file").name("Version").unwrap().as_str() != new_version {
		panic!("Version write to toml file was unsuccessful.");
	}
	if regex_lock_version.captures(&lock_contents).expect("Could not get version number from toml file").name("Version").unwrap().as_str() != new_version {
		panic!("Version write to lock file was unsuccessful.");
	}
}