// auto-publib crate-only

mod auto_publib;
mod auto_update_libs;
mod update_version_number;

pub(crate) use auto_publib::*;
pub(crate) use auto_update_libs::*;
pub(crate) use update_version_number::*;