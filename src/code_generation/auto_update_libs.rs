use crate::{ file_operations::{read_file, write_file}, logging };
use regex::Regex;

/// Update all used libraries to their newest version automatically.
pub fn auto_update_libs(toml_files:&Vec<String>) {
	// TODO: Implement soon! let regex_external_dependency:Regex = Regex::new(r#"\S+(\s+)?=(\s+)\"\d+\.\d+\.\d+\""#).expect("Could not parse regex expression");
	let regex_local_dependency:Regex = Regex::new(r#"\S+(\s+)?=(\s+)\{\spath=\"(?<Path>\S+)\".*version=\"(?<Version>\d+\.\d+\.\d+)\".*\}"#).expect("Could not parse regex expression");
	let regex_git_dependency:Regex = Regex::new(r#"\S+(\s+)?=(\s+)\{\sgit=\"(?<Git>\S+)\".*version=\"(?<Version>\d+\.\d+\.\d+)\".*\}"#).expect("Could not parse regex expression");
	let regex_toml_version:Regex = Regex::new(r#"version(\s+)?=(\s+)?\"(?<Version>\d+\.\d+\.\d+)\""#).expect("Could not parse regex expression");

	// Loop through Cargo.toml files.
	for file in toml_files {
		let file_contents:String = read_file(file);
		let file_lines:Vec<&str> = file_contents.split('\n').collect::<Vec<&str>>();

		// Extract dependencies.
		let mut current_category:String = String::new();
		let mut required_alterations:Vec<[String; 2]> = Vec::new();
		for (line_index, line) in file_lines.iter().enumerate().filter(|(_, line)| !line.trim().is_empty()).collect::<Vec<(usize, &&str)>>() {

			// Only interested in categories with dependencies.
			if line.starts_with('[') && line.ends_with(']') {
				current_category = line.replace(['[', ']'], "");
			}
			if !current_category.contains("dependency") && !current_category.contains("dependencies") { continue; }

			// Create variables to use per dependency type.
			let dependency_name:&str = line.split('=').collect::<Vec<&str>>()[0];
			let mut local_version:Option<String> = None;
			let mut crate_toml:Option<String> = None;

			// Local dependency.
			if let Some(local_captures) = regex_local_dependency.captures(line) {
				let current_version:&str = local_captures.name("Version").unwrap().as_str();
				local_version = Some(current_version.to_owned());
				let source_path:&str = local_captures.name("Path").unwrap().as_str();
				crate_toml = Some(read_file(&format!("{source_path}/Cargo.toml")));
			}

			// Git dependency.
			if let Some(local_captures) = regex_git_dependency.captures(line) {
				let current_version:&str = local_captures.name("Version").unwrap().as_str();
				local_version = Some(current_version.to_owned());
				let source_git_url:String = format!("{}/raw/main/Cargo.toml", local_captures.name("Git").unwrap().as_str().replace(".git", ""));

				// Fetch the contents of Cargo.toml
				match reqwest::blocking::get(&source_git_url) {
					Ok(response) => {
						if response.status().is_success() {
							match response.text() {
								Ok(text) => {
									crate_toml = Some(text);
								},
								Err(_) => logging::warn(&format!("Could not get text from {source_git_url}"), file, line, line_index, 0, line.len())
							}
						}
					},
					Err(_) => logging::warn(&format!("Could not download {source_git_url}"), file, line, line_index, 0, line.len())
				}
			}

			// Compare versions.
			if let Some(local_version) = local_version {
				if let Some(crate_toml) = crate_toml {

					// Try to get crate version from toml.
					if let Some(crate_captures) = regex_toml_version.captures(&crate_toml) {

						// Compare versions.
						let crate_version:&str = crate_captures.name("Version").unwrap().as_str();
						if crate_version != local_version {
							logging::log(&format!("Changing dependency {dependency_name} from version {local_version} to {crate_version}"));
							required_alterations.push([line.to_string(), line.replace(&local_version, crate_version)]);
						}
					}
					
					// Could not get version from toml.
					else {
						logging::warn("Could nog get version from crates toml file.", file, line, line_index, 0, line.len())
					}
				}
			}
		}

		// Make actual updates to file.
		let mut updated_toml_contents:String = file_contents.to_string();
		for alteration in &required_alterations {
			updated_toml_contents = updated_toml_contents.replace(&alteration[0], &alteration[1]);
		}
		write_file(file, &updated_toml_contents);
	}
}