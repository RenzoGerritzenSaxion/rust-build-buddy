type MultiCacheParamSet = (&'static str, &'static dyn Fn(&str) -> bool);

pub struct FileListCache {
	name:String,
	source_dirs:Vec<String>,
	file_matching_function:&'static dyn Fn(&str) -> bool,
	files_cache:Option<Vec<String>>
}
impl FileListCache {

	/// Create a new cache.
	pub fn new(name:&str, working_dirs:&[String], file_matching_function:&'static dyn Fn(&str) -> bool) -> FileListCache {
		FileListCache {
			name: String::from(name),
			source_dirs: working_dirs.to_vec(),
			file_matching_function,
			files_cache: None
		}
	}

	/// Set the source dirs of the cache.
	pub fn set_working_dirs(&mut self, dirs:&[String]) {
		self.source_dirs = dirs.to_vec();
		self.files_cache = None;
	}

	/// Clear the cache.
	pub fn clear(&mut self) {
		self.files_cache = None;
	}

	/// Get all matching files in the source directories.
	pub fn fetch(&mut self) -> Vec<String> {
		use crate::file_operations::enumerate_dir;

		// If there is no cache yet, create it.
		if self.files_cache.is_none() {
			self.files_cache = Some(
				self.source_dirs.iter().flat_map(|dir| {
					enumerate_dir(dir, false, true, true, self.file_matching_function)
				}).collect::<Vec<String>>()
			);
		}
		
		// Return cache.
		self.files_cache.as_ref().unwrap().clone()
	}
}

pub struct MultiFileListCache {
	caches:Vec<FileListCache>
}
impl MultiFileListCache {

	/// Create a new multi-cache.
	pub fn new(source_dirs:&[String], cache_data:&[MultiCacheParamSet]) -> MultiFileListCache {
		MultiFileListCache {
			caches: cache_data.iter().map(|cache_data| FileListCache::new(cache_data.0, source_dirs, cache_data.1)).collect::<Vec<FileListCache>>()
		}
	}

	/// Update the source dirs of the caches.
	pub fn set_working_dirs(&mut self, dirs:&[String]) {
		for cache in &mut self.caches {
			cache.set_working_dirs(dirs);
		}
		self.clear();
	}

	/// Clear all caches.
	pub fn clear(&mut self) {
		for cache in &mut self.caches {
			cache.clear();
		}
	}

	/// Get the value of a specific cache.
	pub fn fetch(&mut self, cache_name:&str) -> Option<Vec<String>> {
		let mut matching_caches:Vec<&mut FileListCache> = self.caches.iter_mut().filter(|cache| cache.name == cache_name).collect::<Vec<&mut FileListCache>>();
		if !matching_caches.is_empty() {
			Some(matching_caches[0].fetch())
		} else {
			None
		}
	}
}