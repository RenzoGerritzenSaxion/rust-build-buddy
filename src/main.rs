use rust_build_buddy::BuildBuddyContext;

fn main() {
	let mut context:BuildBuddyContext = BuildBuddyContext::new();
	
	// Update project files.
	context.auto_update_libs();
	context.generate_auto_public();
	context.validate_code_quality_all();

	// If env var set, update version number.
	if std::env::var("UPDATE_VERSION_NUMBER").unwrap_or_default() == "true" {
		context.update_version_number();
	}
}