/// Checks if a file exists. Works for dirs too.
pub fn file_exists(dir:&str) -> bool {
	std::path::Path::new(&dir).exists()
}

/// Get the filename of a filepath.
pub fn filename_of(path:&str) -> String {
	path.replace('\\', "/").split('/').last().unwrap().to_owned()
}

/// Get the filename of a filepath without extension.
pub fn filename_no_ext_of(path:&str) -> String {
	let file_name:String = filename_of(path);
	if file_name.contains('.') {
		let components:Vec<&str> = file_name.split('.').collect::<Vec<&str>>();
		let last_dot_location:usize = components[0..components.len()-1].iter().map(|component| component.len()).reduce(|a, b| a + b + 1).unwrap();
		file_name[0..last_dot_location].to_owned()
	} else {
		file_name
	}
}

/// List all files/folders in a given dir.
pub fn enumerate_dir(source_dir:&str, list_dirs:bool, list_files:bool, recurse_dirs:bool, match_function:&dyn Fn(&str) -> bool) -> Vec<String> {
	let mut results:Vec<String> = Vec::new();

	// Validate dir exists.
	if file_exists(source_dir) {
		if let Ok(file_reader) = std::fs::read_dir(source_dir) {
			for file in file_reader.flatten() {

				// Parse entry.
				let file_path:String = file.path().display().to_string().replace('\\', "/");
				let file_name:String = filename_of(&file_path);
				let is_dir:bool = !file_name.contains('.');

				// Handle dir.
				if is_dir {
					if list_dirs && match_function(&file_path) {
						results.push(String::from(&file_path));
					}
					if recurse_dirs {
						results.extend_from_slice(&enumerate_dir(&file_path, list_dirs, list_files, recurse_dirs, match_function)[..]);
					}
				}

				// Handle file.
				else if list_files && match_function(&file_path) {
					results.push(String::from(&file_path));
				}
			}
		}
	}

	results
}

/// Read the contents of a given file.
pub fn read_file(file_path:&str) -> String {
	std::fs::read_to_string(file_path).unwrap().replace('\r', "")
}

/// Write the contents to a given file.
pub fn write_file(file_path:&str, contents:&str) {
	use std::io::Write;
	use std::fs::File;
	
	let mut file:File = std::fs::File::create(file_path).unwrap();
	let _ = file.write_all(contents.to_string().as_bytes());
}