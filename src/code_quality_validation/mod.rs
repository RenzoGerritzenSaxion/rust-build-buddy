// auto-publib crate-only

mod comment_spacing;
mod struct_function_categorization;
mod unit_test_location;

pub(crate) use comment_spacing::*;
pub(crate) use struct_function_categorization::*;
pub(crate) use unit_test_location::*;