use crate::{ logging, statics::{ STRUCTURE_OPENING_TAGS, MAX_FUNCTIONS_PER_CATEGORY }, file_operations::read_file, code_parsing::{ find_implementation_bodies, remove_nested } };
use regex::Regex;

/// Check structures are correctly categorized.
pub fn validate_struct_function_categorization(rust_files:&Vec<String>) {
	let function_counter_regex:Regex = Regex::new(r#"\sfn\s"#).expect("Could not parse regex string.");
	let category_counter_regex:Regex = Regex::new(r#"\/\*\s(.+)\sMETHODS\s\*\/"#).expect("Could not parse regex string.");

	// Loop through rust files.
	for rust_file in rust_files {
		let rust_file_contents:String = read_file(rust_file);
		let rust_file_lines:Vec<&str> = rust_file_contents.split('\n').collect::<Vec<&str>>();

		// Continue to next file if no structs are found in the file.
		let mut contains_any:bool = false;
		for tag in &STRUCTURE_OPENING_TAGS {
			if rust_file_contents.contains(tag) {
				contains_any = true;
			}
		}
		if !contains_any { continue; }

		// Find struct bodies.
		let structure_bodies:Vec<[&str; 2]> = find_implementation_bodies(&rust_file_contents);

		// Per body, count functions.
		for [body_title, body] in structure_bodies {
			let body_bottom_level:String = remove_nested(body);
			let function_count:usize = function_counter_regex.captures_iter(&body_bottom_level).count();
			let category_count:usize = category_counter_regex.captures_iter(&body_bottom_level).count().max(1);

			// Validate category ratio.
			let functions_per_category:f32 = function_count as f32 / category_count as f32;
			if functions_per_category > MAX_FUNCTIONS_PER_CATEGORY {

				// Warn user.
				let (source_line_index, source_line) = rust_file_lines.iter().enumerate().filter(|(_, line)| line.contains(body_title)).collect::<Vec<(usize, &&str)>>()[0];
				logging::warn(
					&format!("The implementation for '{rust_file}' '{body_title}' has {function_count} functions, but only {category_count} categories. Maximum functions per category should be {MAX_FUNCTIONS_PER_CATEGORY}."),
					rust_file,
					source_line,
					source_line_index,
					source_line.find(body_title).unwrap_or(0),
					body_title.len()
				);
			}
		}
	}
}