use crate::{ logging, file_operations::read_file };
use regex::Regex;

/// Validate all comments have the correct amount of spacing in front of them.
pub fn validate_comment_spacing(rust_files:&Vec<String>) {
	let regex_doc_string:Regex = Regex::new(r"^\s*\/\/\/.+").expect("Could not parse regex string.");
	let regex_in_line_comment:Regex = Regex::new(r"^\s*\/\/[^\/]?").expect("Could not parse regex string.");
	let regex_multi_line_comment:Regex = Regex::new(r"^\s*\/\*.+\*\/").expect("Could not parse regex string.");
	let regex_category_comment:Regex = Regex::new(r"^\s*\/\*.+METHODS.+\*\/").expect("Could not parse regex string.");
	let regex_empty_line:Regex = Regex::new(r"^\s*$").expect("Could not parse regex string.");
	let regex_use_statement:Regex = Regex::new(r"^\s*use\s").expect("Could not parse regex string.");
	
	// Loop through rust files.
	for rust_file in rust_files {
		let rust_file_contents:String = read_file(rust_file);
		let rust_file_lines:Vec<&str> = rust_file_contents.split('\n').collect::<Vec<&str>>();

		// Loop through file lines.
		for (line_index, line) in rust_file_lines.iter().enumerate() {
			let report_line_index:usize = line_index + 1;

			// Validate spacing of category strings.
			if regex_category_comment.is_match(line) {
				let mut category_unspaced:bool = false;
				if line_index < 3 {
					category_unspaced = true;
				} else {

					// Validate 3 lines in front are empty.
					for offset in 1..4 {
						let offset_line:&str = rust_file_lines[line_index - offset];
						if !regex_empty_line.is_match(offset_line) {
							category_unspaced = true;
						}
						if offset > 1 && offset_line.contains('{') { // First category only requires one newline.
							category_unspaced = false;
							break;
						}
					}

					// Validate line 4 in front is not empty.
					if line_index > 3 && regex_empty_line.is_match(rust_file_lines[line_index - 4]) {
						logging::warn(
							&format!("Incorrect categorization string spacing in '{rust_file}:{report_line_index}', please ensure 3 empty lines in front of the categorization string."),
							rust_file,
							line,
							line_index,
							line.find("/*").unwrap(),
							line.find("*/").unwrap() - line.find("/*").unwrap()
						);
					}
				}
				if category_unspaced {
					logging::warn(
						&format!("Incorrect categorization string spacing in '{rust_file}:{report_line_index}', please ensure 3 empty lines in front of the categorization string."),
						rust_file,
						line,
						line_index,
						line.find("/*").unwrap(),
						line.find("*/").unwrap() - line.find("/*").unwrap()
					);
				}
			}

			// Validate spacing of docstrings.
			else if regex_doc_string.is_match(line) {
				if line_index > 0 {
					let line_before:&str = rust_file_lines[line_index - 1];

					// Second line of comment, previous comment has been parsed.
					if regex_doc_string.is_match(line_before) { continue; }

					// Line is not a todo comment and the line before this one is not empty.
					if !regex_empty_line.is_match(line_before) && !line.to_lowercase().contains("todo") {
						logging::warn(
							"Incorrect docstring spacing in, please add an empty line in front of the docstring.",
							rust_file,
							line,
							line_index,
							line.find("///").unwrap(),
							line.find('\n').unwrap_or(line.len()) - line.find("///").unwrap()
						);
					} else if line_index > 1 {

						// Line 2 back is empty, spacing is too big.
						if regex_empty_line.is_match(rust_file_lines[line_index - 2]) {
							logging::warn(
								"Incorrect docstring spacing, please remove an empty line in front of the docstring.",
								rust_file,
								line,
								line_index,
								line.find("///").unwrap(),
								line.find('\n').unwrap_or(line.len()) - line.find("///").unwrap()
							);
						}
					}
				}
			}

			// Validate spacing of in-line comments.
			else if regex_in_line_comment.is_match(line) {
				if line_index > 0 {
					let line_before:&str = rust_file_lines[line_index - 1];

					// Second line of comment, previous comment has been parsed.
					if regex_in_line_comment.is_match(line_before) { continue; }

					// Line is not a todo comment and the line before this one is not empty.
					if !regex_empty_line.is_match(line_before) && !line.to_lowercase().contains("todo") {
						logging::warn(
							"Incorrect comment spacing, please add an empty line in front of the comment.",
							rust_file,
							line,
							line_index,
							line.find("//").unwrap(),
							line.find('\n').unwrap_or(line.len()) - line.find("//").unwrap()
						);
					}
				}
			}

			// Validate spacing of multi-line comments.
			else if regex_multi_line_comment.is_match(line) {
				if line_index > 0 {
					let line_before:&str = rust_file_lines[line_index - 1];
					if !regex_empty_line.is_match(line_before) {
						logging::warn(
							"Incorrect comment spacing, please add an empty line before the comment.",
							rust_file,
							line,
							line_index,
							line.find("/*").unwrap(),
							line.find("*/").unwrap() - line.find("/*").unwrap()
						);
					}
				}
			}

			// Validate spacing of use expressions.
			else if regex_use_statement.is_match(line) && line_index < rust_file_lines.len() - 1 {
				let next_line:&str = rust_file_lines[line_index + 1];

				// Next line is also a use statement, will be parsed next.
				if regex_use_statement.is_match(next_line) { continue; }

				// Validate next line is empty.
				if !regex_empty_line.is_match(next_line) {
					logging::warn(
						"Incorrect use spacing, please add an empty line after the use statement.",
						rust_file,
						line,
						line_index,
						line.find("use").unwrap(),
						line.find('\n').unwrap_or(line.len()) - line.find("use").unwrap()
					);
				}
			}
		}
	}
}