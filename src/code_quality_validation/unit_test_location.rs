use crate::{ code_parsing::find_between, file_operations::read_file, logging, statics::{ TEST_CONFIG_END_TAG, TEST_CONFIG_START_TAG } };

pub type UnitTestValidationFunction = dyn Fn(&str) -> bool;

/// Require all tests to be located in a specific location.
pub fn validate_unit_test_location(rust_files:&[String], location_validation_function:&UnitTestValidationFunction) {
	
	// Loop through rust files.
	for rust_file in rust_files {
		let rust_file_contents:String = read_file(rust_file);
		let rust_file_lines:Vec<&str> = rust_file_contents.split('\n').collect::<Vec<&str>>();

		// Loop through lines in the file contents.
		for (line_index, line) in rust_file_lines.iter().enumerate() {

			// Check if the line contains a test cfg declaration.
			if line.contains(TEST_CONFIG_START_TAG) && find_between(line, TEST_CONFIG_START_TAG, TEST_CONFIG_END_TAG).unwrap_or_default().contains("test") {
				
				// Validate test is in correct location.
				if !location_validation_function(rust_file) {
					logging::warn(
						"Tests found in incorrect location", 
						rust_file, 
						line, 
						line_index, 
						line.find(TEST_CONFIG_START_TAG).unwrap(), 
						line.find("test").unwrap() + 4
					)
				}
			}
		}
	}
}