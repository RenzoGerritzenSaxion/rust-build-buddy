use regex::Regex;

/// Find the titles and bodies of all implementations.
pub fn find_implementation_bodies(contents:&str) -> Vec<[&str; 2]> {

	// Find all implementation titles.
	let regex_title:Regex = Regex::new(r"\s(trait|impl)\s(?<StructTitle>.+)").unwrap();
	let mut implementation_bodies:Vec<[&str; 2]> = Vec::new();
	for capture in regex_title.captures_iter(contents) {
		let title_capture:regex::Match = capture.name("StructTitle").unwrap();
		let title:&str = &title_capture.as_str()[..title_capture.len()-1];

		// Find the matching body.
		let mut depth:usize = 1;
		let mut char_index:usize = title_capture.end();
		while char_index < contents.len() && depth > 0 {
			match &contents[char_index..char_index+1] {
				"{" => depth += 1,
				"}" => depth -= 1,
				_ => {}
			}
			char_index += 1;
		}
		if char_index - 1 > title_capture.end() {
			implementation_bodies.push([title, &contents[title_capture.end()..char_index-1]]);
		}
	}
	implementation_bodies
}