use regex::Regex;

/// Remove nested code from string.
pub fn remove_nested(contents:&str) -> String {
	let regex:Regex = Regex::new(r#"(\{([^{}]|(?R))*\})"#).expect("Could not parse regex string.");
	let mut output_contents:String = contents.to_string();
	for capture in regex.captures_iter(contents) {
		let nested_contents:&str = capture.get(0).unwrap().as_str();
		output_contents = output_contents.replace(nested_contents, "");
	}
	output_contents
}