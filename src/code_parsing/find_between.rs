/// In a string, find a substring between 2 given strings in.
pub fn find_between(haystack:&str, start:&str, end:&str) -> Option<String> {
	if !haystack.contains(start) { return None; }
	let sub_string:&str = haystack.split(start).collect::<Vec<&str>>()[1];
	if !sub_string.contains(end) { return None; }
	Some(sub_string.split(end).collect::<Vec<&str>>()[0].to_owned())
}