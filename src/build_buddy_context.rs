use crate::{ file_list_cache::MultiFileListCache, logging };

pub struct BuildBuddyContext {
	working_dirs:Vec<String>,
	cache:MultiFileListCache
}
impl BuildBuddyContext {

	/* CONSTRUCTOR METHODS */

	/// Create a new context.
	pub fn new() -> BuildBuddyContext {
		let working_dirs:Vec<String> = vec![String::from("./")];
		BuildBuddyContext {
			working_dirs: working_dirs.clone(),
			cache: MultiFileListCache::new(&working_dirs, &[
				("rust_files", 		&|path:&str| !path.contains("/target/") && path.ends_with(".rs")),
				("rust_scripts", 	&|path:&str| !path.contains("/target/") && path.ends_with(".rs") && !(path.ends_with("mod.rs") || path.ends_with("lib.rs"))),
				("lib_files", 		&|path:&str| !path.contains("/target/") && path.ends_with("lib.rs")),
				("mod_files", 		&|path:&str| !path.contains("/target/") && path.ends_with("mod.rs")),
				("toml_files", 		&|path:&str| !path.contains("/target/") && path.ends_with("Cargo.toml"))
			])
		}
	}



	/* BUILDER METHODS */

	/// Return self with a new working dir.
	pub fn working_dir(mut self, dir:&str) -> BuildBuddyContext {
		self.working_dirs = vec![String::from(dir)];
		self.cache.set_working_dirs(&self.working_dirs);
		self
	}

	/// Return self with a new set of working dirs.
	pub fn working_dirs(mut self, dirs:Vec<&str>) -> BuildBuddyContext {
		self.working_dirs = dirs.iter().map(|dir| String::from(*dir)).collect::<Vec<String>>();
		self
	}



	/* CODE_GENERATION METHODS */

	/// Automatically update dependencies to newest version.
	pub fn auto_update_libs(&mut self) {
		logging::log("Updating dependencies.");
		let toml_files:Vec<String> = self.cache.fetch("toml_files").unwrap();
		crate::code_generation::auto_update_libs(&toml_files);
	}

	/// Automatically create public use statements in mod and lib files.
	pub fn generate_auto_public(&mut self) {

		// Fetch required files.
		let files:Vec<String> = [
			self.cache.fetch("lib_files").unwrap(),
			self.cache.fetch("mod_files").unwrap()
		].iter().flatten().map(|file| file.to_owned()).collect::<Vec<String>>();

		// Generate code.
		logging::log("Generating public use statements for libraries and modules.");
		crate::code_generation::auto_publib(&files);
	}

	/// Update the main crates version number by 0.0.1.
	pub fn update_version_number(&self) {
		logging::log("Updating main crate version number.");
		crate::code_generation::update_version_number();
	}



	/* CODE QUALITY VALIDATION METHODS */

	/// Validate all code quality checks.
	pub fn validate_code_quality_all(&mut self) {
		self.validate_struct_function_categorization();
		self.validate_comment_spacing();
	}

	/// Validate structures are correctly categorized.
	pub fn validate_struct_function_categorization(&mut self) {
		logging::log("Validating categorized structures.");
		let rust_files:Vec<String> = self.cache.fetch("rust_files").unwrap();
		crate::code_quality_validation::validate_struct_function_categorization(&rust_files);
	}

	/// Validate comments are correctly spaced.
	pub fn validate_comment_spacing(&mut self) {
		logging::log("Validating categorized structures.");
		let rust_files:Vec<String> = self.cache.fetch("rust_files").unwrap();
		crate::code_quality_validation::validate_comment_spacing(&rust_files);
	}

	/// Validate unit tests are in correct location.
	pub fn validate_unit_test_location(&mut self, location_validation_function:&crate::code_quality_validation::UnitTestValidationFunction) {
		logging::log("Validating unit test locations.");
		let rust_files:Vec<String> = self.cache.fetch("rust_files").unwrap();
		crate::code_quality_validation::validate_unit_test_location(&rust_files, location_validation_function);
	}



	/* GIT METHODS */

	pub fn create_basic_gitlab_ci_yml(&self) {
		crate::git::create_basic_gitlab_ci_yml();
	}
}

// Implement a default context.
impl Default for BuildBuddyContext {
	fn default() -> Self {
		Self::new()
	}
}

// Once a context drops out of scope, print all logs.
impl Drop for BuildBuddyContext {
	fn drop(&mut self) {
		logging::print();
	}
}